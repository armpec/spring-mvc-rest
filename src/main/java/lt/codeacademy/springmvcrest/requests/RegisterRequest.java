package lt.codeacademy.springmvcrest.requests;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lt.codeacademy.springmvcrest.models.UserRegistration;

@Data
@NoArgsConstructor
public class RegisterRequest {

    private Long companyId;

    private String username;

    @ToString.Exclude
    private String password;

    private String firstName;

    private String lastName;

    private Integer age;

    // for testing only, user should not be able to register as admin
    private Boolean isAdmin;

    public RegisterRequest(UserRegistration userRegistration) {
        this.companyId = userRegistration.getCompanyId();
        this.username = userRegistration.getUsername();
        this.password = userRegistration.getPassword();
        this.firstName = userRegistration.getFirstName();
        this.lastName = userRegistration.getLastName();
        this.age = userRegistration.getAge();
        this.isAdmin = userRegistration.getIsAdmin();
    }
}
