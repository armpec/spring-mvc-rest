package lt.codeacademy.springmvcrest.requests;

import lombok.Data;
import lombok.NoArgsConstructor;
import lt.codeacademy.springmvcrest.models.Car;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class UpdateCarRequest {

    private Long ownerId;

    private String manufacturer;

    private String model;

    private Integer year;

    private String color;

    private String description;

    private BigDecimal value;

    public UpdateCarRequest(Car car) {
        this.ownerId = car.getOwnerId();
        this.manufacturer = car.getManufacturer();
        this.model = car.getModel();
        this.year = car.getYear();
        this.color = car.getColor();
        this.description = car.getDescription();
        this.value = car.getValue();
    }
}
