package lt.codeacademy.springmvcrest.controllers;

import lt.codeacademy.springmvcrest.models.Car;
import lt.codeacademy.springmvcrest.models.User;
import lt.codeacademy.springmvcrest.services.CarService;
import lt.codeacademy.springmvcrest.services.CompanyService;
import lt.codeacademy.springmvcrest.services.UserService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/cars")
public class CarController {

    private final CarService carService;

    private final UserService userService;

    private final CompanyService companyService;

    public CarController(CarService carService, UserService userService, CompanyService companyService) {
        this.carService = carService;
        this.userService = userService;
        this.companyService = companyService;
    }

    @GetMapping
    public String getCars(@RequestParam(required = false) Long companyId, @RequestParam(required = false) String search, Model model) {
        model.addAttribute("cars", carService.getCars(companyId, search));
        model.addAttribute("companies", companyService.getCompanies());
        return "cars/index";
    }

    @GetMapping("/user")
    public String getCarsByUser(Model model, @AuthenticationPrincipal User user) {
        model.addAttribute("cars", userService.getUserCars(user.getId()));
        return "cars/index";
    }

    @GetMapping("/create")
    public String createCar(Model model, @AuthenticationPrincipal User user) {
        model.addAttribute("car", new Car());

        if (user.getIsAdmin()) {
            model.addAttribute("users", userService.getUsers());
        }

        return "cars/create";
    }

    @GetMapping("/{id}/view")
    public String getCar(@PathVariable("id") Long id, Model model) {
        Car car = carService.getCar(id);

        model.addAttribute("car", car);

        if (car.getOwnerId() != null) {
            model.addAttribute("owner", userService.getUser(car.getOwnerId()));
        }

        return "cars/view";
    }

    @GetMapping("/{id}/edit")
    public String editCar(@PathVariable("id") Long id, Model model, @AuthenticationPrincipal User user) {
        model.addAttribute("car", carService.getCar(id));

        if (user.getIsAdmin()) {
            model.addAttribute("users", userService.getUsers());
        }

        return "cars/edit";
    }

    @GetMapping("/{id}/delete")
    public String deleteCar(@PathVariable("id") Long id, RedirectAttributes attributes) {
        carService.deleteCar(id);

        attributes.addFlashAttribute("deleteSuccess", "{cars.delete.success}");

        return "redirect:/";
    }

    @PostMapping("/create")
    public String createCar(@Valid Car car, BindingResult result, Model model, RedirectAttributes attributes,
                            @AuthenticationPrincipal User user) {
        if (result.hasErrors()) {
            if (user.getIsAdmin()) {
                model.addAttribute("users", userService.getUsers());
            }

            return "cars/create";
        }

        car = carService.createCar(car);

        attributes.addFlashAttribute("createSuccess", "{cars.create.success}");

        return "redirect:/cars/" + car.getId() + "/view";
    }

    @PostMapping("/{id}/edit")
    public String editCar(@PathVariable("id") Long id, @Valid Car car, BindingResult result, Model model,
                          @AuthenticationPrincipal User user) {
        if (result.hasErrors()) {
            if (user.getIsAdmin()) {
                model.addAttribute("users", userService.getUsers());
            }

            return "cars/edit";
        }

        return "redirect:/cars/" + carService.editCar(id, car).getId() + "/view";
    }
}
