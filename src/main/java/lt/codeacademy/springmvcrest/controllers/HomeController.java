package lt.codeacademy.springmvcrest.controllers;

import lt.codeacademy.springmvcrest.exceptions.UserAlreadyExistsException;
import lt.codeacademy.springmvcrest.models.User;
import lt.codeacademy.springmvcrest.models.UserLogin;
import lt.codeacademy.springmvcrest.models.UserRegistration;
import lt.codeacademy.springmvcrest.services.CompanyService;
import lt.codeacademy.springmvcrest.services.UserService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class HomeController {

    private final UserService userService;

    private final CompanyService companyService;

    public HomeController(UserService userService, CompanyService companyService) {
        this.userService = userService;
        this.companyService = companyService;
    }

    @GetMapping
    public String home(@AuthenticationPrincipal User user) {
        if (user == null) {
            return "redirect:/login";
        }

        return user.getIsAdmin() ? "redirect:/cars" : "redirect:/cars/user";
    }

    @GetMapping("/login")
    public String login(Model model, @AuthenticationPrincipal User user) {
        if (user == null) {
            model.addAttribute("userLogin", new UserLogin());
            return "login";
        }

        return "redirect:/";
    }

    @GetMapping("/register")
    public String register(Model model, @AuthenticationPrincipal User user) {
        if (user == null) {
            model.addAttribute("userRegistration", new UserRegistration());
            model.addAttribute("companies", companyService.getCompanies());
            return "register";
        }

        return "redirect:/";
    }

    @PostMapping("/register")
    public String register(@Valid UserRegistration userRegistration, BindingResult result, RedirectAttributes attributes) {
        if (result.hasErrors()) {
            return "register";
        }

        if (!userRegistration.getPassword().equals(userRegistration.getConfirmPassword())) {
            result.reject("error.password");
            return "register";
        }

        try {
            userService.register(userRegistration);
        } catch (UserAlreadyExistsException e) {
            result.reject("error.username");
            return "register";
        }

        attributes.addFlashAttribute("registerSuccess", "{register.success}");

        return "redirect:/login";
    }
}
