package lt.codeacademy.springmvcrest.daos;

import lombok.extern.slf4j.Slf4j;
import lt.codeacademy.springmvcrest.models.Car;
import lt.codeacademy.springmvcrest.models.Company;
import lt.codeacademy.springmvcrest.models.User;
import lt.codeacademy.springmvcrest.models.UserRegistration;
import lt.codeacademy.springmvcrest.requests.CreateCarRequest;
import lt.codeacademy.springmvcrest.requests.LoginRequest;
import lt.codeacademy.springmvcrest.requests.RegisterRequest;
import lt.codeacademy.springmvcrest.requests.UpdateCarRequest;
import lt.codeacademy.springmvcrest.responses.CreateCarResponse;
import lt.codeacademy.springmvcrest.responses.LoginResponse;
import lt.codeacademy.springmvcrest.responses.UpdateCarResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.List;

/**
 * Swagger url - http://92.62.139.201:8080/swagger-ui/index.html
 * Postman collection - resources/carAPI.postman_collection.json
 * REST API git repository - https://armpec@bitbucket.org/armpec/spring-rest.git
 * <p>
 * UI -> REST API
 * <p>
 * + GET /cars             -> GET /cars, GET /companies
 * + GET /cars/{id}/view   -> GET /cars/{id}, GET /users/{id}
 * + GET /cars/{id}/edit   -> GET /cars/{id}, GET /users
 * + GET /cars/create      -> GET /users
 * + GET /cars/{id}/delete -> DELETE /cars/{id}
 * + GET /cars/user        -> GET /users/{id}/cars
 * + POST /cars/create     -> POST /cars
 * + POST /cars/{id}/edit  -> PUT /cars/{id}
 * <p>
 * + GET /register         -> GET /companies
 * + POST /login           -> POST /login
 * + POST /register        -> POST /register
 */
@Slf4j
@Component
public class ApiDAO {

    private final RestTemplate restTemplate;

    private final HttpSession session;

    @Value("${url.api}")
    private String url;

    public ApiDAO(RestTemplate restTemplate, HttpSession session) {
        this.restTemplate = restTemplate;
        this.session = session;
    }

    // GET /cars
    public List<Car> getCars(Long companyId, String search) {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth((String) session.getAttribute("accessToken"));

        HttpEntity<?> entity = new HttpEntity<>(headers);
        String url = this.url + "/cars";

        if (companyId != null) {
            url += "?companyId=" + companyId;
        } else if (search != null) {
            url += "?search=" + search;
        }

        log.debug("GET {}", url);

        return Arrays.asList(restTemplate.exchange(url, HttpMethod.GET, entity, Car[].class).getBody());
    }

    // GET /cars/{id}
    public Car getCar(Long id) {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth((String) session.getAttribute("accessToken"));

        HttpEntity<?> entity = new HttpEntity<>(headers);

        log.debug("GET {}/cars/{}", url, id);

        return restTemplate.exchange(url + "/cars/{id}", HttpMethod.GET, entity, Car.class, id).getBody();
    }

    // GET /users
    public List<User> getUsers() {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth((String) session.getAttribute("accessToken"));

        HttpEntity<?> entity = new HttpEntity<>(headers);

        log.debug("GET {}/users", url);

        return Arrays.asList(restTemplate.exchange(url + "/users", HttpMethod.GET, entity, User[].class).getBody());
    }

    // GET /users/{id}
    public User getUser(Long id) {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth((String) session.getAttribute("accessToken"));

        HttpEntity<?> entity = new HttpEntity<>(headers);

        log.debug("GET {}/users/{}", url, id);

        return restTemplate.exchange(url + "/users/{id}", HttpMethod.GET, entity, User.class, id).getBody();
    }

    // GET /users/{id}/cars
    public List<Car> getUserCars(Long id) {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth((String) session.getAttribute("accessToken"));

        HttpEntity<?> entity = new HttpEntity<>(headers);

        log.debug("GET {}/users/{}/cars", url, id);

        return Arrays.asList(restTemplate.exchange(url + "/users/{id}/cars", HttpMethod.GET, entity, Car[].class, id).getBody());
    }

    public List<Company> getCompanies() {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth((String) session.getAttribute("accessToken"));

        HttpEntity<?> entity = new HttpEntity<>(headers);

        log.debug("GET {}/companies", url);

        return Arrays.asList(restTemplate.exchange(url + "/companies", HttpMethod.GET, entity, Company[].class).getBody());
    }

    // POST /cars
    public Car createCar(Car car) {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth((String) session.getAttribute("accessToken"));

        CreateCarRequest createCarRequest = new CreateCarRequest(car);
        HttpEntity<?> entity = new HttpEntity<>(createCarRequest, headers);

        log.debug("POST {}/cars, body = {}", url, createCarRequest);

        return new Car(restTemplate.exchange(url + "/cars", HttpMethod.POST, entity, CreateCarResponse.class).getBody());
    }

    // PUT /cars/{id}
    public Car editCar(Long id, Car car) {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth((String) session.getAttribute("accessToken"));

        UpdateCarRequest updateCarRequest = new UpdateCarRequest(car);
        HttpEntity<?> entity = new HttpEntity<>(updateCarRequest, headers);

        log.debug("PUT {}/cars/{}, body = {}", url, id, updateCarRequest);

        return new Car(restTemplate.exchange(url + "/cars/{id}", HttpMethod.PUT, entity, UpdateCarResponse.class, id).getBody());
    }

    // DELETE /cars/{id}
    public void deleteCar(Long id) {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth((String) session.getAttribute("accessToken"));

        HttpEntity<?> entity = new HttpEntity<>(headers);

        log.debug("DELETE {}/cars/{}", url, id);

        restTemplate.exchange(url + "/cars/{id}", HttpMethod.DELETE, entity, Void.class, id);
    }

    // POST /register
    public User register(UserRegistration userRegistration) {
        RegisterRequest registerRequest = new RegisterRequest(userRegistration);

        log.debug("POST {}/register, body = {}", url, registerRequest);

        return restTemplate.postForObject(url + "/register", registerRequest, User.class);
    }

    // POST /login
    public LoginResponse login(String username, String password) {
        LoginRequest loginRequest = new LoginRequest(username, password);

        log.debug("POST {}/login, body = {}", url, loginRequest);

        return restTemplate.postForObject(url + "/login", loginRequest, LoginResponse.class);
    }
}
