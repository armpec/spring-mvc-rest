package lt.codeacademy.springmvcrest.responses;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class UpdateCarResponse {

    private Long id;

    private Long ownerId;

    private String manufacturer;

    private String model;

    private Integer year;

    private String color;

    private String description;

    private BigDecimal value;

    private String created;

    private String updated;
}
