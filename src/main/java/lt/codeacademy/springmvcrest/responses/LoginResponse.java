package lt.codeacademy.springmvcrest.responses;

import lombok.Data;
import lombok.NoArgsConstructor;
import lt.codeacademy.springmvcrest.models.User;

@Data
@NoArgsConstructor
public class LoginResponse {

    private String accessToken;

    private User user;
}
