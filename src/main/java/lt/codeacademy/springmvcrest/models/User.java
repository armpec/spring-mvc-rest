package lt.codeacademy.springmvcrest.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
public class User {

    private Long id;

    private Long companyId;

    private String username;

    private String firstName;

    private String lastName;

    private Integer age;

    private Set<Role> roles;

    private Boolean isAdmin;

    private String created;

    private String updated;

    public String getFullName() {
        return firstName + " " + lastName;
    }
}
