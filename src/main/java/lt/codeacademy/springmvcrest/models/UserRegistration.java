package lt.codeacademy.springmvcrest.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class UserRegistration {

    @NotNull(message = "{users.company} {error.notnull}")
    private Long companyId;

    @NotBlank(message = "{users.username} {error.notblank}")
    private String username;

    @Size(min = 6, max = 30, message = "{users.password} {error.size}")
    private String password;

    @Size(min = 6, max = 30, message = "{users.password} {error.size}")
    private String confirmPassword;

    @NotBlank(message = "{users.firstName} {error.notblank}")
    private String firstName;

    @NotBlank(message = "{users.lastName} {error.notblank}")
    private String lastName;

    @NotNull(message = "{users.age} {error.notnull}")
    @Range(min = 1, max = 140, message = "{users.age} {error.range}")
    private Integer age;

    // for testing only, user should not be able to register as admin
    private Boolean isAdmin = true;
}
