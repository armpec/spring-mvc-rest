package lt.codeacademy.springmvcrest.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import lt.codeacademy.springmvcrest.responses.CreateCarResponse;
import lt.codeacademy.springmvcrest.responses.UpdateCarResponse;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class Car {

    private Long id;

    private Long ownerId;

    private Company company;

    @NotBlank(message = "{cars.manufacturer} {error.notblank}")
    private String manufacturer;

    @NotBlank(message = "{cars.model} {error.notblank}")
    private String model;

    @NotNull(message = "{cars.year} {error.notnull}")
    @Range(min = 1900, max = 2100, message = "{cars.year} {error.range}")
    private Integer year;

    @NotBlank(message = "{cars.color} {error.notblank}")
    private String color;

    @NotBlank(message = "{cars.description} {error.notblank}")
    private String description;

    @NotNull(message = "{cars.value} {error.notnull}")
    @Positive(message = "{cars.value} validation.positive}")
    private BigDecimal value;

    private String created;

    private String updated;

    public Car(CreateCarResponse createCarResponse) {
        this.id = createCarResponse.getId();
        this.ownerId = createCarResponse.getOwnerId();
        this.manufacturer = createCarResponse.getManufacturer();
        this.model = createCarResponse.getModel();
        this.year = createCarResponse.getYear();
        this.color = createCarResponse.getColor();
        this.description = createCarResponse.getDescription();
        this.value = createCarResponse.getValue();
        this.created = createCarResponse.getCreated();
        this.updated = createCarResponse.getUpdated();
    }

    public Car(UpdateCarResponse updateCarResponse) {
        this.id = updateCarResponse.getId();
        this.manufacturer = updateCarResponse.getManufacturer();
        this.model = updateCarResponse.getModel();
        this.year = updateCarResponse.getYear();
        this.color = updateCarResponse.getColor();
        this.description = updateCarResponse.getDescription();
        this.value = updateCarResponse.getValue();
        this.created = updateCarResponse.getCreated();
        this.updated = updateCarResponse.getUpdated();
    }
}
