package lt.codeacademy.springmvcrest.models;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Company {

    private Long id;

    private String name;

    private String created;

    private String updated;
}
