package lt.codeacademy.springmvcrest.services;

import lt.codeacademy.springmvcrest.daos.ApiDAO;
import lt.codeacademy.springmvcrest.models.Car;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarService {

    private final ApiDAO apiDAO;

    public CarService(ApiDAO apiDAO) {
        this.apiDAO = apiDAO;
    }

    public List<Car> getCars(Long companyId, String search) {
        return apiDAO.getCars(companyId, search);
    }

    public Car getCar(Long id) {
        return apiDAO.getCar(id);
    }

    public Car createCar(Car car) {
        return apiDAO.createCar(car);
    }

    public Car editCar(Long id, Car car) {
        return apiDAO.editCar(id, car);
    }

    public void deleteCar(Long id) {
        apiDAO.deleteCar(id);
    }
}
