package lt.codeacademy.springmvcrest.services;

import lt.codeacademy.springmvcrest.daos.ApiDAO;
import lt.codeacademy.springmvcrest.models.Company;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {

    private final ApiDAO apiDAO;

    public CompanyService(ApiDAO apiDAO) {
        this.apiDAO = apiDAO;
    }

    public List<Company> getCompanies() {
        return apiDAO.getCompanies();
    }
}
