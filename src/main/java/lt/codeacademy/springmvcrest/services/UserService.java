package lt.codeacademy.springmvcrest.services;

import lt.codeacademy.springmvcrest.daos.ApiDAO;
import lt.codeacademy.springmvcrest.exceptions.UserAlreadyExistsException;
import lt.codeacademy.springmvcrest.models.Car;
import lt.codeacademy.springmvcrest.models.User;
import lt.codeacademy.springmvcrest.models.UserRegistration;
import lt.codeacademy.springmvcrest.responses.LoginResponse;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

@Service
public class UserService {

    private final ApiDAO apiDAO;

    public UserService(ApiDAO apiDAO) {
        this.apiDAO = apiDAO;
    }

    public User register(UserRegistration userRegistration) {
        try {
            return apiDAO.register(userRegistration);
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.CONFLICT)) {
                throw new UserAlreadyExistsException(userRegistration.getUsername());
            }

            throw e;
        }
    }

    public LoginResponse login(String username, String password) {
        return apiDAO.login(username, password);
    }

    public List<User> getUsers() {
        return apiDAO.getUsers();
    }

    public User getUser(Long id) {
        return apiDAO.getUser(id);
    }

    public List<Car> getUserCars(Long id) {
        return apiDAO.getUserCars(id);
    }
}
