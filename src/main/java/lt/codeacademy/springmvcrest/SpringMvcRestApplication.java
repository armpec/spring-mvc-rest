package lt.codeacademy.springmvcrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Swagger url - http://92.62.139.201:8080/swagger-ui/index.html
 * Postman collection - resources/carAPI.postman_collection.json
 * REST API git repository - https://armpec@bitbucket.org/armpec/spring-rest.git
 */
@SpringBootApplication
public class SpringMvcRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringMvcRestApplication.class, args);
    }

}
